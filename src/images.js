const images = [
    {
        id: 1,
        src: 'https://images.theconversation.com/files/350865/original/file-20200803-24-50u91u.jpg?ixlib=rb-1.1.0&q=45&auto=format&w=1200&h=675.0&fit=crop',
        likes: 333
    },
    {
        id: 2,
        likes: 12,
        src: 'https://ichef.bbci.co.uk/news/1024/branded_news/68DF/production/_109474862_angrycat-index-getty3-3.jpg',
    },
    {
        id: 3,
        likes: 1,
        src: 'https://trupanion.com/-/media/trupanion/images/breed--guide--new/bengal/bengal-cat.jpg?la=en&hash=6BF0EEDC7DDCDDE7C9184D44BA9B2920F345BFA2',
    },
    {
        id: 4,
        likes: 122,
        src: 'https://www.yourcat.co.uk/images/legacy/catimages/Breed_Bengal/bengalkitten.jpg',
    },
    {
        id: 5,
        likes: 0,
        src: 'https://cattime.com/assets/uploads/gallery/bengal-cats/bengal-5.jpg',
    },
    {
        id: 6,
        likes: 121,
        src: 'https://a.allegroimg.com/original/1e2e7f/2214aade4031a4f8a00b000d55e1',
    },
    {
        id: 7,
        likes: 13,
        src: 'https://abcbirds.org/wp-content/uploads/2015/03/cat-and-bird_Zanna-Holstova_SS.jpg',
    },

];
export default images;