import React from 'react';
import images from './images'

import './Slider.css';
import 'react-toastify/dist/ReactToastify.css';

import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {
    faLessThan,
    faGreaterThan,
    faThumbsDown,
    faThumbsUp } from '@fortawesome/free-solid-svg-icons';
import {ToastContainer, toast} from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

const Slider = () => {

    const [imagesHook, setImagesHook] = React.useState(images);
    const [currentImage, setCurrentImage] = React.useState(0);
    const [liked, setLiked] = React.useState(false);
    const [disliked, setDisLiked] = React.useState(false);
    const likesAmount = imagesHook[currentImage].likes;

    React.useEffect(() => {
        let localStorageImage = localStorage.getItem('_images');

        toast.info('🦄 Klikając przycisk klawiaturowy prawo/lewo ' +
            'możesz przeglądać zdjęcia!', {
            position: "top-right",
            autoClose: 5000,
            hideProgressBar: false,
            closeOnClick: true,
            pauseOnHover: true,
            draggable: true,
            progress: undefined,
        });

        if (localStorageImage) setImagesHook(JSON.parse(localStorageImage));
        else localStorage.setItem('_images', JSON.stringify(images));

    }, []);

    React.useEffect(() => {
        document.addEventListener("keydown", handleKeyPress, false);
        return () => {
            document.removeEventListener("keydown", handleKeyPress, false);
        };
    }, []);

    const handleKeyPress = React.useCallback((event) => {
        if (event.key === "ArrowLeft") {
            setCurrentImage(current => current > 0 ? current - 1 : current);
        }
        if (event.key === "ArrowRight") {
            setCurrentImage(current => current < imagesHook.length - 1 ? current + 1 : current);
        }
    }, [currentImage]);


    React.useEffect(() => {
        localStorage.setItem('_images', JSON.stringify(imagesHook));
    }, [currentImage]);


    function actionWithImage(typeOfAction) {
        if (typeOfAction === "+") setLiked(true);
        if (typeOfAction === "-") setDisLiked(true);

        setImagesHook((oldImages) => {
            typeOfAction === '+' ?
                imagesHook[currentImage].likes = imagesHook[currentImage].likes + 1
                :
                imagesHook[currentImage].likes = imagesHook[currentImage].likes - 1

            localStorage.setItem('_images', JSON.stringify(imagesHook));
            return [
                ...oldImages
            ]
        })
    }

    return (
        <>
            <ToastContainer/>
            <div onKeyPress={e => handleKeyPress(e)} className={'photo-container'}>

                <div className={'previous-foto-wrapper'}>
                    {currentImage > 0 &&
                    <img
                        alt=''
                        className={'previous-foto'}
                        src={imagesHook[currentImage - 1].src}/>
                    }
                </div>

                <div className={'current-foto-wrapper'}>
                    {currentImage !== 0 &&
                    <FontAwesomeIcon
                        icon={faLessThan}
                        className={'arrow arrow-back'}
                        onClick={() => setCurrentImage(currentImage - 1)}/>
                    }
                    <img
                        className={'current-foto'}
                        alt=''
                        src={imagesHook[currentImage].src
                        }/>
                    {currentImage < imagesHook.length - 1 &&
                    <FontAwesomeIcon className={'arrow arrow-next'}
                                     onClick={() => setCurrentImage(currentImage + 1)}
                                     icon={faGreaterThan}/>
                    }
                </div>

                <div className={'next-foto-wrapper'}>
                    {currentImage !== imagesHook.length - 1 &&
                    <img
                        className={'next-foto'}
                        alt=''
                        src={imagesHook[currentImage + 1].src}/>
                    }
                </div>

            </div>

            <div className={'likes-container'}>

                {likesAmount !== 0
                &&
                <FontAwesomeIcon
                    className={`dislike ${disliked && 'disliked-animation'}`}
                    onClick={() => actionWithImage("-")}
                    icon={faThumbsDown}
                />}

                <div
                    className={'like-counter'}>
                    {likesAmount !== 0 && "+"}
                    {likesAmount}
                </div>

                <FontAwesomeIcon
                    className={`like ${liked && 'liked-animation'}`}
                    onClick={() => actionWithImage("+")}
                    icon={faThumbsUp}/>
            </div>
        </>
    );
}

export default Slider;
